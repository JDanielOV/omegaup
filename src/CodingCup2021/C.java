package CodingCup2021;

import java.util.Scanner;

public class C {
    public static void main(String[] args) {
        int iCasos, numerador = 0, denominador = 0;
        Scanner scan = new Scanner(System.in);
        iCasos = scan.nextInt();
        for (int i = 0; i < iCasos; i++) {
            numerador = scan.nextInt();
            denominador = scan.nextInt();
            String sReMi = reduccionMinima(numerador, denominador);
            if (reduccionAntonio(numerador, denominador).equals(sReMi)) {
                System.out.println(sReMi.concat(" SI"));
            } else {
                System.out.println(sReMi.concat(" NO"));
            }
        }
        /*System.out.println(reduccionAntonio(numerador, denominador));
        System.out.println(reduccionMinima(numerador, denominador));;*/

    }

    public static String reduccionAntonio(int numerador, int denominador) {
        StringBuilder sNumerador = new StringBuilder(String.valueOf(numerador));
        StringBuilder sDenominador = new StringBuilder(String.valueOf(denominador));
        int iPos = 0;
        for (int i = 0; i < sNumerador.length() && sNumerador.length() > 1 && sDenominador.length() > 1; i++) {
            int iAnt = i - 1, iDes = i + 1;
            if (iAnt >= 0 && sDenominador.charAt(iAnt) == sNumerador.charAt(i)) {
                sNumerador.deleteCharAt(i);
                sDenominador.deleteCharAt(iAnt);
                i = -1;
            } else if (iDes < sDenominador.length() && sDenominador.charAt(iDes) == sNumerador.charAt(i)) {
                sNumerador.deleteCharAt(i);
                sDenominador.deleteCharAt(iDes);
                i = -1;
            }
        }
        return sNumerador.toString().concat("/").concat(sDenominador.toString());
    }

    public static String reduccionMinima(int numerador, int denominador) {
        int menor = Math.max(numerador, denominador);
        for (int i = 2; i < menor && i <= numerador && i <= denominador; i++) {
            while (numerador % i == 0 && denominador % i == 0) {
                numerador = numerador / i;
                denominador = denominador / i;
            }
        }
        String valor = numerador + "/" + denominador;
        return valor;
    }
}
