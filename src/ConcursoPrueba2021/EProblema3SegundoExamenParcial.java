package ConcursoPrueba2021;
import java.util.Scanner;
import java.util.ArrayList;

public class EProblema3SegundoExamenParcial {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        ArrayList<Integer>arrayList=new ArrayList<>();
        int iMatrix=scan.nextInt(),iValor,iSuma=0;
        for (int i = 0; i < iMatrix; i++) {
            for (int j = 0; j < iMatrix; j++) {
                iValor=scan.nextInt();
                arrayList.add(iValor);
                if(i==j)
                    iSuma+=iValor;
            }
        }
        int iContador=0;
        System.out.println(iSuma);
        for (int i = 0; i < iMatrix; i++) {
            for (int j = 0; j < iMatrix; j++) {
                System.out.print(arrayList.get(iContador)+" ");
                iContador++;
            }
            System.out.println();
        }

    }
}
