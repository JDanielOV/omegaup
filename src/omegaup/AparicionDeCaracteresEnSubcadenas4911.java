package omegaup;

import java.util.Scanner;

public class AparicionDeCaracteresEnSubcadenas4911 {
    public static void main(String[] args) {
        AparicionDeCaracteresEnSubcadenas4911();
    }

    public static void AparicionDeCaracteresEnSubcadenas4911() {
        Scanner scan = new Scanner(System.in);
        StringBuilder sWord = new StringBuilder(scan.nextLine());
        int iCases = scan.nextInt(), iBeggin, iEnd;
        char cLetter;
        for (int i = 0; i < iCases; i++) {
            cLetter = scan.next().charAt(0);
            iBeggin = scan.nextInt();
            iEnd = scan.nextInt();
            String sResult=sWord.substring(iBeggin, iBeggin + iEnd).contains(""+cLetter)?"1":"0";
            System.out.println(sResult);
        }
    }
}
