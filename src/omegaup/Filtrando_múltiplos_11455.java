/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package omegaup;

import java.util.Scanner;

/**
 *
 * @author Daniel Ochoa
 */
public class Filtrando_múltiplos_11455 {

    public static void Filtrando_múltiplos_11455() {
        Scanner scan = new Scanner(System.in);
        int iCasos = scan.nextInt();
        int iValue;
        int iArr[] = new int[iCasos];
        StringBuilder sReturn = new StringBuilder();
        for (int i = 0; i < iCasos; i++) {
            iArr[i] = scan.nextInt();
        }
        iValue = scan.nextInt();
        for (int i : iArr) {
            if (i % iValue == 0) {
                sReturn.append(i + " ");
            } else {
                sReturn.append("X ");
            }
        }
        System.out.println(sReturn.deleteCharAt(sReturn.length() - 1));
    }
}
