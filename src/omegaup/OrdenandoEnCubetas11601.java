package omegaup;

import java.util.Scanner;
import java.util.HashMap;

public class OrdenandoEnCubetas11601 {
    public static void main(String[] args) {
        OrdenandoEnCubetas11601();
    }

    public static void OrdenandoEnCubetas11601() {
        int iPelotas, iMaximo, iValores;
        Scanner scan = new Scanner(System.in);
        HashMap<Integer, Integer> mMap = new HashMap<Integer, Integer>();
        iPelotas = scan.nextInt();
        iMaximo = scan.nextInt();
        for (int i = 0; i < iPelotas; i++) {
            iValores = scan.nextInt();
            if (mMap.containsKey(iValores)) {
                mMap.put(iValores, mMap.get(iValores) + 1);
            } else {
                mMap.put(iValores, 1);
            }
        }
        for (int i = 1; i <= iMaximo; i++) {
            if (mMap.containsKey(i)) {
                System.out.println(i + ": " + mMap.get(i));
            } else {
                System.out.println(i + ": " + 0);
            }
        }
    }
}
