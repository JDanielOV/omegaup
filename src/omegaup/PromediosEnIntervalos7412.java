package omegaup;

import java.util.Scanner;
import java.util.HashMap;

public class PromediosEnIntervalos7412 {
    public static void main(String[] args) {
        PromediosEnIntervalos7412();
    }

    public static void PromediosEnIntervalos7412() {
        Scanner scan = new Scanner(System.in);
        int iCasos = scan.nextInt(), iLista, iValor;
        HashMap<Integer, Integer> mMap = new HashMap<>();
        for (int i = 0; i < iCasos; i++) {
            iLista = scan.nextInt();
            iValor = scan.nextInt();
            mMap.put(iLista, iValor);
        }
        iCasos = scan.nextInt();
        for (int i = 0; i < iCasos; i++) {
            iLista = scan.nextInt();
            iValor = scan.nextInt();
            int iPromedio = 0, iAlumnos = 0;
            boolean bExiste = false;
            for (int j = iLista; j <= iValor; j++) {
                if (mMap.containsKey(j)) {
                    bExiste = true;
                    iPromedio += mMap.get(j);
                    iAlumnos++;
                }
            }
            if (bExiste) {
                if (iAlumnos > 0) {
                    System.out.println(iPromedio / iAlumnos);
                } else {
                    System.out.println(0);
                }
            } else {
                System.out.println(-1);
            }
        }
    }
}
